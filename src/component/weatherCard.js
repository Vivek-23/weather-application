import { Component } from "react";
import './css/weatherCard.css';

export default class WeatherCard extends Component {
    render() {
        const { weatherCard, city } = this.props;
        let img_url = `http://openweathermap.org/img/w/${weatherCard.icon}.png`

        return (
            <div className="row">
                <div className="col-1">
                    <h1>{city}</h1>
                    <h2>{weatherCard.date}</h2>
                    <h2>{weatherCard.day}</h2>
                    <img src={img_url} alt="" />
                </div>
                <div className="col">
                    <div className="col1">
                        <h2>{weatherCard.max_temp}<span className="celcius">&#8451;</span></h2>
                        <h5>Max:</h5>

                        <h2>{weatherCard.min_temp}<span className="celcius">&#8451;</span></h2>
                        <h5>Min:</h5>

                    </div>
                    <div className="col2">
                        <h2>{weatherCard.windSpeed} <span>km/hr</span></h2>
                        <h5>Wind Speed:</h5>

                        <h2>{weatherCard.pressure} <span>hpa</span></h2>
                        <h5>Pressure:</h5>

                    </div>
                    <div className="col3">
                        <h2>{weatherCard.weather}</h2>
                        <h5>Weather:</h5>

                        <h2>{weatherCard.humidity}<span className="percentage">%</span></h2>
                        <h5>Humidity:</h5>

                    </div>
                </div>
            </div>
        )
    }
}