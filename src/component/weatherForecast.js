import { Component } from "react";
import './css/weatherForecast.css';

export default class WeatherForecast extends Component {
    render() {
        const {weatherValue} = this.props;
        let img_url = `http://openweathermap.org/img/w/${weatherValue.icon}.png`
        return(
            <div className="card-container">
                <div className="date">
                    <h4>{weatherValue.date}</h4>
                </div>
                <div className="time">
                    <h4>{weatherValue.time}</h4>
                </div>
                <div className="weather-icon">
                    <img src={img_url} alt=""/>
                </div>
                <div className="temperature">
                    <h4>{weatherValue.max_temp}<span className="celcius-symbol">&#8451;</span></h4>
                </div>
            </div>  
        )
    }
}

