
function findWeatherDetails(weatherList) {
    let week_day = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
    let weatherInfo = [];
    let weatherObj = Object.entries(weatherList.list);

    Array.prototype.forEach.call(weatherObj, (curr) => {
        let dateDetails = new Date(curr[1].dt_txt);
        let time = dateDetails.getHours() + ":" + dateDetails.getUTCMinutes();
        let date = dateDetails.getDate() + " " + month[dateDetails.getMonth()];
        let max_temp = curr[1]["main"].temp_max;
        let min_temp = curr[1]["main"].temp_min;
        let icon = curr[1]["weather"][0].icon;
        let weather = curr[1]["weather"][0].main;
        let windSpeed = (curr[1]["wind"].speed) * 3.6;
        let day = week_day[dateDetails.getDay()];
        let pressure = curr[1]["main"].pressure;
        let humidity = curr[1]["main"].humidity;

        let weatherObj = {
            date: date,
            time: time,
            max_temp: max_temp,
            min_temp: min_temp,
            icon: icon,
            weather: weather,
            windSpeed: windSpeed.toFixed(2),
            day: day,
            pressure: pressure,
            humidity: humidity
        }

        weatherInfo.push(weatherObj);
    });

    return weatherInfo;
}

module.exports = findWeatherDetails;
