import { Component } from 'react';
import './App.css';
import WeatherForecast from './component/weatherForecast';
import WeatherCard from './component/weatherCard';
import findWeatherDetails from './component/weatherDetails';
import axios from 'axios';
import { ClipLoader } from 'react-spinners';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      city: "",
      weatherData: {},
      message: ""
    }
  }

  fetchApi = async (city) => {
    try {
      const url = `${process.env.REACT_APP_WEATHER_URL}?q=${city||this.state.city}&APPID=${process.env.REACT_APP_WEATHER_ID}&units=metric`
      const response = await axios.get(url);
      const jsonResponse = response.data;
      return findWeatherDetails(jsonResponse);
    } catch (err) {
      throw Error(err.response.data.message);
    }
  }

  handleInputSearch = (event) => {
    this.setState({
      search: event.target.value
    });
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(async (position) => {
      let longitude = position.coords.longitude;
      let latitude = position.coords.latitude;
      let response = await axios.get(`${process.env.REACT_APP_WEATHER_URL}?lat=${latitude}&lon=${longitude}&appid=${process.env.REACT_APP_WEATHER_ID}&units=metric`);
      let weatherData = findWeatherDetails(response.data);
      this.setState({
        city: response.data.city.name,
        weatherData: weatherData,
        message: ""
      });
    }, () => {
      this.setState({
        message: "Enter your city name"
      });
    });
  }

  handleSubmitForm = async (event) => {
    event.preventDefault();
    try {
      let response = await this.fetchApi(this.state.search);
      this.setState({
        search: event.target.value,
        city: this.state.search || this.state.city,
        weatherData: response,
        message: ""
      });
    } catch (err) {
      this.setState({
        message: err.message
      });
    }
  }

  render() {
    const { weatherData } = this.state;
    return (
      <div className="container">
        <div className="header">
          <h1>Weather Application</h1>
        </div>

        <form className="form" onSubmit={this.handleSubmitForm}>
          <input type="search" placeholder="Enter your city" className="inputFeild" onChange={this.handleInputSearch} />
          <br />
          <button type="submit">Search</button>
        </form>
        {(this.state.message !== "") ?
          (<h1>{this.state.message}</h1>) : (
            (Object.keys(weatherData).length === 0) ? (
              <div className="loading">
                <ClipLoader color={"black"} loading={true} />
              </div>
            ) : (
              <>
                <WeatherCard weatherCard={weatherData[0]} city={this.state.city} />
                <div className="weather-container">
                  {
                    Array.prototype.map.call(weatherData, (item) => {
                      return (
                        <>
                          <WeatherForecast className="weather-forecast" weatherValue={item} />
                        </>
                      )
                    })}
                </div>
              </>
            )
          )}
      </div>
    )
  }
}


export default App;
